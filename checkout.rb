RULES = {"A" => [[1, 50], [3, 130]],
		 "B" => [[1, 30], [2, 45]],
		 "C" => [[1, 20]],
		 "D" => [[1, 15]]}
class  CheckOut

	
	def initialize(rules)
		@rules=rules
		@total = 0
		@compras ={}
		@valorPorGrupoDeProdutos = {}
	end

	def soma(item)
		
		if @rules[item].length == 1
			begin  
				@valorPorGrupoDeProdutos[item] = @valorPorGrupoDeProdutos[item] + @rules[item][0][1] # se o item ja estiver na llista de compra adicione mais uma compra
			rescue 
    			@valorPorGrupoDeProdutos.[]=(item, @rules[item][0][1]) #se nao adicione o item a lista de compras {"item" => quantidade}
			end		
		else
			regreDeTarifacaoDoProduto = @rules[item] 
			tamanho = regreDeTarifacaoDoProduto.length - 1
			soma = 0
			divisor = @compras[item]

			for i in (0..tamanho).to_a.reverse
				if (divisor < regreDeTarifacaoDoProduto[i][0] and divisor != 0)
					next
				end
					
				cocienteEresto = divisor.divmod regreDeTarifacaoDoProduto[i][0]
				divisor = cocienteEresto[1]
				soma = soma + (cocienteEresto[0] * regreDeTarifacaoDoProduto[i][1])
						
			end
			begin
				valorPorGrupoDeProdutos[item].length == 1 
				@valorPorGrupoDeProdutos[item] = soma
			rescue	
				@valorPorGrupoDeProdutos.[]=(item, soma) #se nao adicione o item a lista de compras {"item" => quantidade}

			end
		end

		@total = @valorPorGrupoDeProdutos.values.inject(:+)
	end

	def total() #soma e retorna o valor das compra
		@total

	end

	def scan(item) # conta a quantidades de cada item comprado

		begin  
			@compras[item] = @compras[item] + 1 # se o item ja estiver na llista de compra adicione + 1
		rescue 
    		@compras.[]=(item, 1) #se nao adicione o item a lista de compras {"item" => quantidade}
		end
		soma(item)

	end

end